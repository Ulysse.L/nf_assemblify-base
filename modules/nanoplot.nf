process nanoplot {

    label 'lowmem'

    tag "QC_${fq_ch.baseName}"

    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/01c_nanoplot",      mode: 'copy', pattern: '*.png'
    publishDir "${params.resultdir}/01c_nanoplot",      mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/logs/nanoplot",	mode: 'copy', pattern: 'nanoplot*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'nanoplot*.cmd'

    input:
        path(fq_ch)
    
    output:
        path("*.*")
        path("*.log")
        path("*.cmd")

    script:
    """
    nanoplot.sh $fq_ch nanoplot_${fq_ch.baseName}.cmd >& nanoplot_${fq_ch.baseName}.log 2>&1
    """ 
}
