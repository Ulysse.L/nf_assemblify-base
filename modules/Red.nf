process Red {

    label 'lowmem'

    tag "Red"

    publishDir "${params.resultdir}/03a_Red",		mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/Red",		mode: 'copy', pattern: 'Red*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'Red*.cmd'

    input:
	path(assembly_fa)
    
    output:
        path("masked/*.msk"), emit: red_ch
        path("Red*.log")
        path("Red*.cmd")

    script:
    """
    Red.sh ${task.cpus} ${assembly_fa} Red.cmd >& Red.log 2>&1
    """ 
}
