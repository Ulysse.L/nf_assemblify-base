# About the assemblify-base pipeline

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).

Following content should be adapted to turn this README as something FAIR to describe the pipeline. Work to be done by training attendees.

# The project
## Introduction :
The main goal is to create a nextflow pipeline for genome assembly and annotation of _Dunaliella primolecto_ genome, a haploid species, with different tools. A pipeline example was therefore given to us, and we added several tools to it. The second goal is to develop the pipeline with the FAIR principles (Findable, Accessible, Interoperable, Reusable). The FAIR principle is therefore composed of 4 concepts, allowing the good management of projects, codes, pipelines, and publications, and was established by the bioinformatic community. We have therefore ensured that our pipeline meets the following needs : it should be functional, distributable, findable, having a documentation and an appropriate licence.

All works were made on the IFB Core Cluster, with use of image docker, and linked with a Gitlab repository. During the pipeline testing, a subset of the dataset will be used, while the entire genome will be used once the totality of the pipeline is correct. The dataset genome is composed of seven files, and the subset is composed of two files.

The full data is the following :

![Full_table_genome](./Image_for_Readme/Full_table_genome.JPG?raw=true)

The subset data is the following :

![Subset_table_genome](./Image_for_Readme/Subset_table_genome.JPG?raw=true)

The pipeline stages are the next. It is to highlight that we dont have implementde all the tools that appear in the pipeline overview, so we have stop to the stage 3 RepeatMasking. The tools that have already been implemented are in colour, and the other the tools that we have to be implemented as much as possible in black.

![pipeline_schema](./Image_for_Readme/pipeline_schema.JPG?raw=true)


## Tools and data accession :
Here is the list of tools with version and input / ouput file that we used into the pipeline.

![Tool_table](./Image_for_Readme/Tool_table.JPG?raw=true)

Here are the docker image for each tools, they are also available on the biocontainer website (https://biocontainers.pro/registry) :
- Nanoplot - quay.io/biocontainers/nanoplot:1.32.1--py_0
- FastQC - quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1
- Multiqc - quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0
- Hifiasm - quay.io/biocontainers/hifiasm:0.18.9--h5b5514e_0
- Busco - quay.io/biocontainers/busco:5.5.0--pyhdfd78af_0
- Red - quay.io/biocontainers/red:2018.09.10--h4ac6f70_2

All the data description can be found on the NCBI database :
https://www.ncbi.nlm.nih.gov/datasets/genome/GCA_914767535.2/

All of our work on the nextflow pipeline can be found at the Gitlab repository and on Zenodo :
- Gitlab - https://gitlab.com/Ulysse.L/nf_assemblify-base
- Zenodo - 10.5281/zenodo.10601214


## Getting started :
All the work we did was on the IFB Core Cluster, in January 2024.  IFB Core Cluster have the folowing computer capacities in January 2024 :
- 103 computer nodes
- 8434 CPU Hyper-threaded
- 55 TB of RAM
- 2 PB of fast storage
- 2 PB of regular storage

For the pipeline, you can find all the configuration in the « ressources.config file » that precise the cpus, memory and time parameters for each process. These processes are defined in each tool configuration and can be modified. There are 3 labels:
- The first one is the « lowmem » who will allocate 2 cpus, 8.GB and 4 hour for the tool to work.
- The second is the « midmem » who will allocate 14 cpus, 24.GB and 8 hour for the tool to work.
- And the last is the « highmem » who will allocate 14 cpus, 64.GB and 12 hour for the tool to work.

We therefore recommend to launch the pipeline on a calcul cluster, with a SLURM (Simple Linux Utility for Resource Management) system. The connection between the Gitlab and the IFB Core Cluster can be made by a SSH key. 

## Usage :
In order to launch properly the pipeline, you have to make a clone of the repository in your working directory. Be careful, you have to change the working directory in the « run_assemblify.slurm » file. Once this change is done, you can launch the pipeline with this command line :
	sbatch run_assemblify.slurm
« run_assemblify.slurm » is a script used to submit the execution of a pipeline on a slurm cluster. 

## Contributing & feedback :
If you want to contribute to the project, here is a short tutorial to see how to do it :
- Create a new fork of the project on the gitlab project.
- Create your Feature Branch :
	git checkout -b feature/AmazingFeature
- Commit your changes :
	git commit -m 'Add some AmazingFeature'
- Push to the Branch :
	git push origin feature/AmazingFeature
- Open a Pull Request

If you want to make a feedback :
- Create an issue to suggest an improvement
- Create a merge request to fix an error or add an improvement: scroll to the bottom of the specific docs page you are looking at and click Edit this page
- Show and post comments at the bottom of an individual docs page to review and give feedback about the page

## Licence :
FAIR principles applied to Genome Assembly and Annotation © 2024 by LE CLANCHE Ulysse, SALE Jebril is licensed under CC BY-NC-SA 4.0 

## Contact :
- Ulysse LE CLANCHE - ulysse.leclanche@etudiant.univ-rennes.fr
- Jébril SALÉ - jebril.sale@etudiant.univ-rennes.fr


## References :
Andrews, S. (2024). S-andrews/FastQC [Java]. https://github.com/s-andrews/FastQC (Édition originale 2017)

chhylp123. (2024). Chhylp123/hifiasm [C++]. https://github.com/chhylp123/hifiasm (Édition originale 2019)

Ewels, P., Magnusson, M., Lundin, S., & Käller, M. (2016). MultiQC : Summarize analysis results for multiple tools and samples in a single report. Bioinformatics, 32(19), 3047‑3048. https://doi.org/10.1093/bioinformatics/btw354

Coster, W. D. (2024). Wdecoster/NanoPlot [Python]. https://github.com/wdecoster/NanoPlot (Édition originale 2017)

Manni, M., Berkeley, M. R., Seppey, M., & Zdobnov, E. M. (2021). BUSCO : Assessing Genomic Data Quality and Beyond. Current Protocols, 1(12), e323. https://doi.org/10.1002/cpz1.323

Palmer, J. (2023). Nextgenusfs/redmask [Python]. https://github.com/nextgenusfs/redmask (Édition originale 2018)


## Instructors :
All images come from the « FAIR principles applied to Genome Assembly and Annotation » course, in bioinformatics master 2 – Rennes - 2024, made by the follows instructors :

- Anthony Bretaudeau - INRAE - Rennes
- Alexandre Cormier - Ifremer, IRSI-SeBiMER, 29280, Plouzané, France
- Erwan Corre - CNRS, Sorbonne Université, FR2424, ABiMS, Station Biologique, 29680, Roscoff, France
- Patrick Durand - Ifremer, IRSI-SeBiMER, 292800, Plouzané, France
- Gildas Le Corguillé - Sorbonne Université, CNRS, FR2424, ABiMS, Station Biologique, 29680, Roscoff, France
- Stéphanie Robin - INRAE - Rennes
