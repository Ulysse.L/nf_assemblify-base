#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                     Genome assembly repeats softmasking                   ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/Red.nf process
args=("$@")
NCPUS=${args[0]}
ASSEMBLY_FILE=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="Red -gnm . -msk masked -cor ${NCPUS}"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Creat directory
mkdir -p masked

# Execute command
eval ${CMD}
