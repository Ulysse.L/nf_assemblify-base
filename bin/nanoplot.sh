#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##       Quality Control, Plotting tool for long read sequencing data.       ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/nanoplot.nf process
args=("$@")
FASTQ=${args[0]}
LOGCMD=${args[1]}

# Command to execute
CMD="NanoPlot --plots kde dot hex --N50 \
         --title \"PacBio Hifi reads for ${FASTQ}\" \
         --fastq ${FASTQ} -o ."

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

